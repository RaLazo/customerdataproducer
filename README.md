# Technology Base of a Data Mesh Node
This repository contains configurtation as well as application code to spin up a simple approach of a Data Mesh Node.
- [Technology Base of a Data Mesh Node](#technology-base-of-a-data-mesh-node)
  - [Dependcies](#dependcies)
  - [Setup](#setup)
  - [Questions](#questions)
  
## Dependcies
The following services are need for the PoC:
- [Kubernetes](https://kubernetes.io/de/docs/tasks/tools/install-minikube/)
- [Docker](https://docs.docker.com/get-docker/)
  
## Setup

The following link will provide you to the either local existing setup guides for the service or the remote documentation:
- [ISTIO - Service Mesh](https://istio.io/latest/docs/setup/getting-started/)
- [Apache Kafka](https://strimzi.io/quickstarts/)
- [Apache Flink](./flink-kubernetes/README.md)
- [MongoDB](./mongodb-kubernetes/README.md)
- [CustomerDataProducer](./customerDataProducer/README.md)

## Questions
If you have any question regarding this application don`t hesitate to contact us:
+ Rafael Lazenhofer
  + rafaellazenhofer@gmail.com
