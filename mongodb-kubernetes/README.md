# MongoDB on Kubernetes 

Setup for MongoDB on Kubernetes.

- [MongoDB on Kubernetes](#mongodb-on-kubernetes)
  - [Setup](#setup)
    - [kubectl apply commands in order](#kubectl-apply-commands-in-order)
    - [kubectl get commands](#kubectl-get-commands)
    - [kubectl debugging commands](#kubectl-debugging-commands)
    - [give a URL to external service in minikube](#give-a-url-to-external-service-in-minikube)
  - [Questions](#questions)

## Setup

### kubectl apply commands in order

```bash   
    kubectl apply -f mongo-secret.yaml
    kubectl apply -f mongo.yaml
    kubectl apply -f mongo-configmap.yaml 
    kubectl apply -f mongo-express.yaml
```

### kubectl get commands

```bash
    kubectl get pod
    kubectl get pod --watch
    kubectl get pod -o wide
    kubectl get service
    kubectl get secret
    kubectl get all | grep mongodb
```

### kubectl debugging commands

```bash
    kubectl describe pod mongodb-deployment-xxxxxx
    kubectl describe service mongodb-service
    kubectl logs mongo-express-xxxxxx
```
### give a URL to external service in minikube

```bash
    minikube service mongo-express-service
```

## Questions
If you have any question regarding this application don`t hesitate to contact us:
+ Rafael Lazenhofer
  + rafaellazenhofer@gmail.com