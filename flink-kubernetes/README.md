# Apache Flink Setup for Kubernetes

instructions for MongoDB on Kubernetes
- [Apache Flink Setup for Kubernetes](#apache-flink-setup-for-kubernetes)
  - [Setup](#setup)
    - [kubectl apply commands in order](#kubectl-apply-commands-in-order)
    - [kubectl get commands](#kubectl-get-commands)
    - [kubectl debugging commands](#kubectl-debugging-commands)
    - [forward flink Web UI in minikube](#forward-flink-web-ui-in-minikube)
  - [Questions](#questions)


## Setup

### kubectl apply commands in order

```bash
  kubectl create -f flink-configuration-configmap.yaml
  kubectl create -f jobmanager-service.yaml
  kubectl create -f jobmanager-session-deployment.yaml
  kubectl create -f jobmanager-session-deployment-ha.yaml
  kubectl create -f taskmanager-session-deployment.yaml
  kubectl create -f jobmanager-session-deployment-non-ha.yaml.yaml
  kubectl create -f jobmanager-session-deployment-non-ha.yaml
```
### kubectl get commands

``` bash
    kubectl get pod
    kubectl get pod --watch
    kubectl get pod -o wide
    kubectl get service
```

### kubectl debugging commands

```bash
    kubectl describe pod mongodb-deployment-xxxxxx
    kubectl describe service mongodb-service
    kubectl logs mongo-express-xxxxxx
```

### forward flink Web UI in minikube

```bash
    kubectl port-forward <jobmanagerServiceName> 8081:8081
```

## Questions
If you have any question regarding this application don`t hesitate to contact us:
+ Rafael Lazenhofer
  + rafaellazenhofer@gmail.com