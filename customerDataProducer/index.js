// CustomerDataProducer
// fetchs random user data for further analysis


const axios = require('axios').default;
const { Kafka } = require('kafkajs')


const USER_API_URL = process.env.USER_API_URL || "https://randomuser.me/api/";
const CDP_CLIENT_ID = process.env.CDP_CLIENT_ID || "customer-data-producer";
const KAFKA_BROKER_URL = process.env.KAFKA_BROKER_URL || "my-cluster-kafka-bootstrap:9092";
const TOPIC_NAME = process.env.TOPIC_NAME || "my-topic";
const REGISTRATION_TIMER = process.env.REGISTRATION_TIMER || 2000;

const kafka = new Kafka({
  clientId: CDP_CLIENT_ID,
  brokers: [ KAFKA_BROKER_URL ],
})

const producer = kafka.producer()

function sleep(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
   }
   

const startCustomerDataProducer = async() => {
    try  {
        await producer.connect()
        while(true) {
            const res = await axios.get(USER_API_URL)
            await producer.send({
                topic: TOPIC_NAME,
                messages: [
                  { value: JSON.stringify(res.data) },
                ],
            })
            await sleep(REGISTRATION_TIMER);
        }   
        
    }catch(error){
        console.log(error)
    }
    await producer.disconnect()
}



console.log("Starting CustomerDataProducer")
startCustomerDataProducer()