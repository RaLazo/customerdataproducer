# Customer Data Producer
This application simulates the registration of a social media portal. 
It produces multiple customer data in a given interval to a `kafka` cluster.

- [Customer Data Producer](#customer-data-producer)
  - [How to use it ?](#how-to-use-it-)
  - [Development](#development)
    - [Configuraton, enviroment and dependencies](#configuraton-enviroment-and-dependencies)
    - [Run it locally](#run-it-locally)
    - [Docker](#docker)
  - [Questions](#questions)


## How to use it ?
With the following commands you can apply the the application to you `kubernetes` cluster.

## Development

This chapter guides you throw the local usage and development of this application

### Configuraton, enviroment and dependencies

To run this script you need to setup a local `kafka` cluster for testing pruposes.
for more information regarding this visit [this link](https://kafka.apache.org/quickstart).

this are the enviorment variables that are possible to set:

```bash
# all of this have a default option 
USER_API_URL= <fakeUserData>
CDP_CLIENT_ID= <randomID>
KAFKA_BROKER_URL= <url>:<port> 
TOPIC_NAME= <topicForData>
REGISTRATION_TIMER=<timeInMiliseconds>
```

### Run it locally
After you set everything up you can just simply type in the following commands to run the application locally:

```bash
# installing dependencies for application
npm install
# running the application
npm run start
```

### Docker

Following steps needs to be accomplished to build a docker image for this application:

```bash
# build your application
docker build -t cdp .

# tag your version to specfic one (give a name)
docker tag cdp:latest ralazo/customerdataproducer:latest

# push to remote registry 
docker push ralazo/customerdataproducer:latest
```

## Questions
If you have any question regarding this application don`t hesitate to contact us:
+ Rafael Lazenhofer
  + rafaellazenhofer@gmail.com